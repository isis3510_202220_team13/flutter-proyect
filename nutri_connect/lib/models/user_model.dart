import 'dart:core';

import 'package:nutri_connect/Services/database_service.dart';
import 'package:nutri_connect/locator.dart';

import 'FriendStorage.dart';

class UserModel {
  String name = "";
  List<String> diets = [];
  String email = "";
  List<String> friends = [];
  String imagePath = '';

  UserModel(
      {required this.name,
      required this.diets,
      required this.email,
      required this.friends,
      required this.imagePath});

  String addDiets(String dietToAdd) {
    String ans = "failure";
    if (!diets.contains(dietToAdd)) {
      diets = diets + [dietToAdd];
      diets.removeWhere((value) => value == "");
      ans = "Success";
    }
    return ans;
  }

  String removeDiets(String dietToRemove) {
    String ans = 'Failure';
    if (diets.contains(dietToRemove)) {
      diets.remove(dietToRemove);
      diets.removeWhere((value) => value == "");
      ans = "Success";
    }
    return ans;
  }

  String giveImagePath() {
    if (imagePath == '') {
      return 'https://firebasestorage.googleapis.com/v0/b/nutri-connected.appspot.com/o/User_IMAGE1670174990637.jpg?alt=media&token=c36db5ac-be12-4435-9567-e9c42629beec';
    }
    return imagePath;
  }

  String addFriend(String friendToAdd) {
    String ans = "failure";
    String friendName = friendToAdd.substring(0, friendToAdd.indexOf(':'));
    String friendEmail =
        friendToAdd.substring(friendToAdd.indexOf(':') + 1, friendToAdd.length);
    if (!friends.contains(friendName)) {
      friends = friends + [friendToAdd];
      friends.removeWhere((value) => value == "");
      ans = "Success";
      print(friendEmail);
      locator.get<FriendStorage>().addEmail(friendEmail);
    }
    return ans;
  }

  Future<Map<String, dynamic>?> datos() async {
    var temp = await locator.get<DatabaseService>().getUserComplete(email);
    return temp;
  }

  Future<String> refresh() async {
    friends.removeWhere((value) => value == "");
    Map<String, dynamic>? tempMap = await datos();
    print(tempMap);
    if (tempMap == null) {
      return 'done';
    }
    List<String> tempDiets = List<String>.from(tempMap['diets'] as List);
    List<String> tempFriends = List<String>.from(tempMap['friends'] as List);
    name = tempMap['name'].toString();
    diets = tempDiets;
    email = tempMap['email'].toString();
    friends = tempFriends;
    imagePath = tempMap['image'].toString();
    return 'done';
  }

  String changeName(String newName) {
    String ans = "Success";
    name = newName;
    return ans;
  }

  String changeEmail(String newEmail) {
    String ans = "Success";
    email = newEmail;
    return ans;
  }

  String changeImage(String newImage) {
    String ans = "Success";
    imagePath = newImage;
    return ans;
  }
}
