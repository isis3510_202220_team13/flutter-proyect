class Food {
  late String barcode;
  late String name;
  late var image;

  Food({required this.barcode, required this.name, required this.image});
}
