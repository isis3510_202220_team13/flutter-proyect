import 'dart:collection';
import 'package:nutri_connect/Services/database_service.dart';
import '../locator.dart';
import 'Friend.dart';

class FriendStorage {
  HashMap<String, Friend> friendMap = HashMap();
  List<String> friendEmails = [];

  void populate() async {
    Friend allOfThem = Friend(diet: '', name: '', image: '');
    Map<String, dynamic>? mapita = <String, dynamic>{};
    for (int i = 0; i < friendEmails.length; i++) {
      mapita =
          await locator.get<DatabaseService>().getUserComplete(friendEmails[i]);
      allOfThem.diet = mapita!['diets'].toString();
      allOfThem.image = mapita['image'];
      allOfThem.name = mapita['name'];
      friendMap[allOfThem.name] = allOfThem;
    }
  }

  void addEmail(String email) async {
    friendEmails = friendEmails + [email];
    Map<String, dynamic>? mapita = <String, dynamic>{};
    mapita = await locator.get<DatabaseService>().getUserComplete(email);
    Friend toAdd = Friend(
        name: mapita!['name'],
        diet: mapita['diets'].toString(),
        image: mapita['image']);
    friendMap[toAdd.name] = toAdd;
  }
}
