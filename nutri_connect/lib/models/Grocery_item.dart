class GroceryItem {
  String name = "";
  int price = 0;
  List<String> tags = [];

  GroceryItem({required this.name, required this.price, required this.tags});
}
