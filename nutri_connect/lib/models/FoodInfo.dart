import 'dart:collection';

import 'package:nutri_connect/models/Morsel.dart';

class FoodStorage {
  HashMap<String, Food> ProductMap = HashMap();

  void addFood(Food morsel) {
    String code = morsel.barcode;
    ProductMap[code] = morsel;
  }

  Food? giveFood(String barcode) {
    if (ProductMap.containsKey(barcode)) {
      return ProductMap[barcode];
    }
    return Food(
        barcode: 'Slipspace rupture', name: 'Failure', image: 'no success');
  }
}
