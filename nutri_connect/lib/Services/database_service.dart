import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:nutri_connect/locator.dart';
import 'package:nutri_connect/models/user_model.dart';

class DatabaseService {
  Future<String?> addUser(
      {required String name,
      required List<String> diet,
      required String email,
      required List<String> friends,
      required String image}) async {
    try {
      CollectionReference users =
          FirebaseFirestore.instance.collection('Users');
      // Call the user's CollectionReference to add a new user
      await users.doc(email).set({
        'diets': diet,
        'email': email,
        'friends': friends,
        'image': image,
        'name': name
      });
      return 'success';
    } catch (e) {
      return 'Error adding user';
    }
  }

  Future<String?> getUser(String email) async {
    try {
      CollectionReference users =
          FirebaseFirestore.instance.collection('Users');
      final snapshot = await users.doc(email).get();
      final data = snapshot.data() as Map<String, dynamic>;
      return data['name'];
    } catch (e) {
      return 'Error fetching user';
    }
  }

  Future<Map<String, dynamic>?> getUserComplete(String email) async {
    try {
      CollectionReference users =
          FirebaseFirestore.instance.collection('Users');
      final snapshot = await users.doc(email).get();
      final data = snapshot.data() as Map<String, dynamic>;
      return data;
    } catch (e) {
      return null;
    }
  }

  Future<String> changeInfo(String key, var toAdd) async {
    try {
      Map<String, dynamic> temp = <String, dynamic>{};
      temp[key] = toAdd;
      CollectionReference users =
          FirebaseFirestore.instance.collection('Users');
      users.doc(locator.get<UserModel>().email).update(temp);
      return toAdd;
    } catch (e) {
      return 'Error fetching user';
    }
  }

  Future uploadInfo() async {
    bool isConnected = await checkUserConnection();
    UserModel modelo = locator.get<UserModel>();
    if (isConnected) {
      changeInfo('diets', modelo.diets);
      changeInfo('name', modelo.name);
      return true;
    }
    return false;
  }

  Future checkUserConnection() async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        return true;
      }
    } on SocketException catch (_) {
      return false;
    }
  }

  Future<String?> getUserForFind(BuildContext cont, String email) async {
    try {
      bool b = await checkUserConnection();
      if (!b) {
        showDialog(
            context: cont,
            builder: (cont) => AlertDialog(
                    title: const Text("Oops!"),
                    content: const Text(
                        "You do not have an internet connection! Try again later."),
                    actions: [
                      TextButton(
                          child: const Text("Understood!"),
                          onPressed: () {
                            Navigator.pop(cont);
                          }),
                    ]));

        return "No connection";
      }
      locator.get<UserModel>().refresh();
      CollectionReference users =
          FirebaseFirestore.instance.collection('Users');
      final snapshot = await users.doc(email).get();
      final data = snapshot.data() as Map<String, dynamic>;
      String nom = data['name'];
      String em = data['email'];
      String toSave = '$nom:$em';
      if (em == locator.get<UserModel>().email) {
        showDialog(
            context: cont,
            builder: (cont) => AlertDialog(
                    title: const Text("Oops!"),
                    content: const Text("That's you!"),
                    actions: [
                      TextButton(
                          child: const Text("Oh, right!"),
                          onPressed: () {
                            Navigator.pop(cont);
                          }),
                    ]));
        return data['name'];
      }
      showDialog(
          context: cont,
          builder: (cont) => AlertDialog(
                  title: const Text("A friend has been found!"),
                  content: Text(
                      "Your friend $nom has been found! Would you like to add them to your friends list?"),
                  actions: [
                    TextButton(
                        child: const Text("Yes!"),
                        onPressed: () {
                          String a = locator.get<UserModel>().addFriend(toSave);
                          Navigator.pop(cont);
                          if (a == "Success") {
                            showDialog(
                                context: cont,
                                builder: (cont) => AlertDialog(
                                        title: const Text(
                                            "Friend added successfully!"),
                                        content: const Text(
                                            "You have a new friend!"),
                                        actions: [
                                          TextButton(
                                              child: const Text("Thank you!"),
                                              onPressed: () {
                                                Navigator.pop(cont);
                                              }),
                                        ]));
                            changeInfo(
                                "friends", locator.get<UserModel>().friends);
                          } else {
                            showDialog(
                                context: cont,
                                builder: (cont) => AlertDialog(
                                        title: const Text("Oh?"),
                                        content: const Text(
                                            "This person is already your friend!"),
                                        actions: [
                                          TextButton(
                                              child:
                                                  const Text("That's great!"),
                                              onPressed: () {
                                                Navigator.pop(cont);
                                              }),
                                        ]));
                          }
                        }),
                    TextButton(
                        child: const Text("No!"),
                        onPressed: () {
                          Navigator.pop(cont);
                        }),
                  ]));
      return data['name'];
    } catch (e) {
      showDialog(
          context: cont,
          builder: (cont) => AlertDialog(
                  title: const Text("A friend has not been found!"),
                  content: const Text(
                      "A profile with that email cannot be found in our database."),
                  actions: [
                    TextButton(
                        child: const Text("Understood!"),
                        onPressed: () {
                          Navigator.pop(cont);
                        }),
                  ]));
      return 'Error fetching user';
    }
  }
}
