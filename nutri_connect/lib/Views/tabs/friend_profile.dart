import 'package:flutter/material.dart';
import 'package:nutri_connect/Services/database_service.dart';

import '../../controllers/log_in_controller.dart';
import '../reusable_widgets/reusable.dart';
import '../screens/loading_screen.dart';
import '../utils/color_find.dart';

class FriendTab extends StatefulWidget {
  late Map<String, dynamic> email;
  FriendTab({Key? key, required this.email}) : super(key: key);

  @override
  _FriendTabState createState() => _FriendTabState();
}

class _FriendTabState extends State<FriendTab> {
  late List<bool> diet;
  @override
  void initState() {
    super.initState();
    diet = values(widget.email["diets"]);
  }

  FirebaseAuthHelper helper = FirebaseAuthHelper();
  bool isLoading = false;

  @override
  Widget build(BuildContext context) => isLoading
      ? LoadingScreen()
      : Scaffold(
          appBar: AppBar(
            title: const Text('Friend'),
            backgroundColor: hexStringToColor("60992D"),
          ),
          backgroundColor: hexStringToColor("60992D"),
          body: SingleChildScrollView(
              child: SafeArea(
            minimum: const EdgeInsets.only(top: 100),
            child: Column(
              children: <Widget>[
                buildImage(widget.email['image']),
                Text(
                  widget.email["name"],
                  style: const TextStyle(
                    fontSize: 40.0,
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontFamily: "Pacifico",
                  ),
                ),

                const SizedBox(
                  height: 20,
                  width: 200,
                  child: Divider(
                    color: Colors.white,
                  ),
                ),

                // we will be creating a new widget name info card
                CheckboxListTile(
                  value: diet[0],
                  title: const Text(
                    'Vegan',
                    style: TextStyle(fontSize: 20),
                  ),
                  onChanged: null,
                ),
                CheckboxListTile(
                  value: diet[1],
                  title: const Text('Veggie', style: TextStyle(fontSize: 20)),
                  onChanged: null,
                ),
                CheckboxListTile(
                  value: diet[2],
                  title: const Text(
                    'Paleo',
                    style: TextStyle(fontSize: 20),
                  ),
                  onChanged: null,
                ),
                CheckboxListTile(
                  value: diet[3],
                  title: const Text('Keto', style: TextStyle(fontSize: 20)),
                  onChanged: null,
                ),
              ],
            ),
          )));
  Future<Map<String, dynamic>?> fiendData(String email) async {
    return await DatabaseService().getUserComplete(email);
  }

  List<bool> values(List<dynamic> test) {
    return [
      test.contains("Vegan"),
      test.contains("Veggie"),
      test.contains("Paleo"),
      test.contains("Keto")
    ];
  }
}
