import 'package:flutter/material.dart';
import 'package:nutri_connect/Services/database_service.dart';
import 'package:nutri_connect/Views/screens/diets.dart';
import 'package:nutri_connect/Views/screens/edit_profile.dart';
import 'package:nutri_connect/Views/screens/loading_green.dart';
import 'package:nutri_connect/controllers/profile_controller.dart';
import 'package:nutri_connect/locator.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../controllers/log_in_controller.dart';
import '../../models/user_model.dart';
import '../reusable_widgets/reusable.dart';
import '../screens/friends.dart';
import '../screens/login_view.dart';
import '../utils/color_find.dart';

class ProfileTab extends StatefulWidget {
  const ProfileTab({Key? key}) : super(key: key);

  @override
  _ProfileTabState createState() => _ProfileTabState();
}

class _ProfileTabState extends State<ProfileTab> {
  @override
  void initState() {
    super.initState();
  }

  FirebaseAuthHelper helper = FirebaseAuthHelper();
  bool isLoading = false;

  @override
  Widget build(BuildContext context) => isLoading
      ? LoadingGreen()
      : Scaffold(
          appBar: AppBar(
            title: const Text('Profile'),
            backgroundColor: hexStringToColor("60992D"),
          ),
          backgroundColor: hexStringToColor("60992D"),
          body: SingleChildScrollView(
              child: SafeArea(
            minimum: const EdgeInsets.only(top: 100),
            child: Column(
              children: <Widget>[
                ProfileWidget(
                    imagePath: locator.get<UserModel>().giveImagePath(),
                    isEdit: false,
                    onclicked: () async {
                      bool internet = await locator
                          .get<DatabaseService>()
                          .checkUserConnection();
                      if (internet) {
                        showDialog(
                            context: context,
                            builder: (cont) => AlertDialog(
                                    title: const Text("Change profile picture"),
                                    content: const Text(
                                        "Would you like to pick a picture from your gallery or from your camera?"),
                                    actions: [
                                      TextButton(
                                          child: const Text('Gallery'),
                                          onPressed: () async {
                                            setState(() => isLoading = true);
                                            Navigator.pop(cont);
                                            await locator
                                                .get<ProfileController>()
                                                .pickUploadImage();
                                            setState(() => isLoading = false);
                                          }),
                                      TextButton(
                                          child: const Text('Camera'),
                                          onPressed: () async {
                                            setState(() => isLoading = true);
                                            Navigator.pop(cont);
                                            String resp = await locator
                                                .get<ProfileController>()
                                                .takeUploadImage();
                                            setState(() => isLoading = false);
                                            if (resp == 'failed') {}
                                          }),
                                      TextButton(
                                          child: const Text('Cancel'),
                                          onPressed: () {
                                            Navigator.pop(cont);
                                          }),
                                    ]));
                      } else {
                        showDialog(
                            context: context,
                            builder: (cont) => AlertDialog(
                                    title:
                                        const Text("No internet connection!"),
                                    content: const Text(
                                        "Without an internet connection, you cannot pick a profile picture. Try again later."),
                                    actions: [
                                      TextButton(
                                          child: const Text('Got it'),
                                          onPressed: () {
                                            Navigator.pop(cont);
                                          }),
                                    ]));
                      }
                    }),

                Text(
                  locator.get<UserModel>().name,
                  style: const TextStyle(
                    fontSize: 40.0,
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontFamily: "Pacifico",
                  ),
                ),

                const SizedBox(
                  height: 20,
                  width: 200,
                  child: Divider(
                    color: Colors.white,
                  ),
                ),

                // we will be creating a new widget name info carrd

                InfoCard(
                    text: "Edit your profile",
                    icon: Icons.info,
                    onPressed: () async {
                      locator.get<UserModel>().refresh();
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => EditProfilePage()));
                      setState(() {});
                    }),
                InfoCard(
                    icon: Icons.child_care,
                    text: "Your friends",
                    onPressed: () async {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const Friends()));
                      setState(() {});
                    }),
                InfoCard(
                    text: "Your dietary restrictions",
                    icon: Icons.food_bank_rounded,
                    onPressed: () async {
                      locator.get<UserModel>().refresh();
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const Diets()));
                    }),
                Padding(
                  padding: const EdgeInsets.fromLTRB(25, 0, 25, 0),
                  child: firebaseUIButton(context, "Sign out?", () async {
                    bool up = await locator.get<DatabaseService>().uploadInfo();
                    if (up) {
                      locator.get<FirebaseAuthHelper>().logout();

                      Navigator.of(context, rootNavigator: true)
                          .pushAndRemoveUntil(
                        MaterialPageRoute(
                          builder: (BuildContext context) {
                            return const SignInScreen();
                          },
                        ),
                        (_) => false,
                      );
                    } else {
                      showDialog(
                          context: context,
                          builder: (cont) => AlertDialog(
                                  title: const Text(
                                      "You have connectivity issues!"),
                                  content: const Text(
                                      "If you sign out while you have connectivity issues, you may lose any changes to your name or diets you have made locally. You can save them locally, sign out without saving, or cancel."),
                                  actions: [
                                    TextButton(
                                        child: const Text("Save locally"),
                                        onPressed: () {
                                          SharedPreferences.getInstance().then(
                                            (prefs) {
                                              prefs.setString(
                                                  "user",
                                                  locator
                                                      .get<UserModel>()
                                                      .name);
                                              prefs.setString(
                                                  'diets',
                                                  locator
                                                      .get<UserModel>()
                                                      .diets
                                                      .toString()
                                                      .replaceAll('[', '')
                                                      .replaceAll(']', ''));
                                            },
                                          );
                                          Navigator.pop(cont);
                                          locator
                                              .get<FirebaseAuthHelper>()
                                              .logout();

                                          Navigator.of(context,
                                                  rootNavigator: true)
                                              .pushAndRemoveUntil(
                                            MaterialPageRoute(
                                              builder: (BuildContext context) {
                                                return const SignInScreen();
                                              },
                                            ),
                                            (_) => false,
                                          );
                                        }),
                                    TextButton(
                                        child:
                                            const Text("Exit without saving"),
                                        onPressed: () {
                                          Navigator.pop(cont);
                                          locator
                                              .get<FirebaseAuthHelper>()
                                              .logout();

                                          Navigator.of(context,
                                                  rootNavigator: true)
                                              .pushAndRemoveUntil(
                                            MaterialPageRoute(
                                              builder: (BuildContext context) {
                                                return const SignInScreen();
                                              },
                                            ),
                                            (_) => false,
                                          );
                                        }),
                                    TextButton(
                                        child: const Text("Cancel"),
                                        onPressed: () {
                                          Navigator.pop(cont);
                                        }),
                                  ]));
                    }
                  }),
                ),
              ],
            ),
          )));
}
