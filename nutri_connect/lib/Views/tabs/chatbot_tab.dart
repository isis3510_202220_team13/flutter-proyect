import 'package:flutter/material.dart';
import 'package:nutri_connect/Views/screens/chatbot.dart';

import '../utils/color_find.dart';

class ChatbotScreenTab extends StatelessWidget {
  const ChatbotScreenTab({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
            appBar: AppBar(
              title: const Text('Chatbot'),
              backgroundColor: hexStringToColor("60992D"),
            ),
            body: Builder(builder: (BuildContext context) {
              Future<String> respuesta;
              return Container(
                  alignment: Alignment.center,
                  child: Flex(
                      direction: Axis.vertical,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        ElevatedButton(
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => ChatBotHome()));
                            },
                            child: const Text('Chatbot')),
                        const Text('Welcome', style: TextStyle(fontSize: 20))
                      ]));
            })));
  }
}
