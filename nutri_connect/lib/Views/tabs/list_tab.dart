import 'package:flutter/material.dart';
import 'package:nutri_connect/Views/screens/groceryList.dart';

import '../utils/color_find.dart';

class ListScreenTab extends StatelessWidget {
  const ListScreenTab({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
            appBar: AppBar(
              title: const Text('Grocery list'),
              backgroundColor: hexStringToColor("60992D"),
            ),
            body: Builder(builder: (BuildContext context) {
              Future<String> respuesta;
              return Container(
                  alignment: Alignment.center,
                  child: Flex(
                      direction: Axis.vertical,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        ElevatedButton(
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          const GroceryList()));
                            },
                            child: const Text('Grocery list')),
                        const Text('Welcome', style: TextStyle(fontSize: 20))
                      ]));
            })));
  }
}
