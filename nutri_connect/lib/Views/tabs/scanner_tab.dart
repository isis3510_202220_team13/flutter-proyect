import 'package:flutter/material.dart';
import 'package:nutri_connect/Views/reusable_widgets/reusable.dart';
import '../../controllers/scanning_controller.dart';
import '../utils/color_find.dart';

class ScannerScreenTab extends StatelessWidget {
  const ScannerScreenTab({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
            backgroundColor: hexStringToColor("60992D"),
            appBar: AppBar(
              title: const Text('Barcode scan'),
              backgroundColor: hexStringToColor("60992D"),
            ),
            body: Builder(builder: (BuildContext context) {
              return Container(
                  alignment: Alignment.center,
                  child: Flex(
                      direction: Axis.vertical,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Padding(
                            padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                            child: firebaseUIButton(
                                context,
                                'Start barcode scan',
                                () => {
                                      scanBarcodeNormal(context),
                                    })),
                      ]));
            })));
  }
}
