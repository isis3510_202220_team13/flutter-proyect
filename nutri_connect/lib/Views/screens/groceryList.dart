import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nutri_connect/Views/screens/home_screen.dart';
import 'package:nutri_connect/Views/utils/color_find.dart';
import '../../models/Grocery_item.dart';
import 'add-grocery.dart';

class GroceryList extends StatefulWidget {
  const GroceryList({super.key});

  @override
  State<GroceryList> createState() => GroceryListState();
}

class GroceryListState extends State<GroceryList> {
  int index = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: const Text('Grocery list'),
          backgroundColor: hexStringToColor("60992D"),),
        bottomNavigationBar: BottomNavigationBar(
          onTap: (int index) {
            if (index != this.index) {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => HomeScreen()));
            }
          },
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
                icon: Icon(Icons.shopping_cart), label: 'Cart'),
            BottomNavigationBarItem(
                icon: Icon(Icons.qr_code_scanner), label: 'Scan'),
          ],
        ),
        body: BlocProvider(
            //Crea el Cubit, el estado
            create: (_) => ListCubit(),
            child: BlocBuilder<ListCubit, List<GroceryItem>>(
                //Refresca el componente para mostrar la información en cambio de estado
                buildWhen: (previousState, state) {
              return true;
            }, builder: (context, state) {
              return Column(
                children: [
                  Center(
                      child: ListView(
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    children: getTextWidgets(state),
                  )),
                  Text('Total value \$${getPrice(state)}',
                      style: const TextStyle(
                          fontSize: 30, fontWeight: FontWeight.bold)),
                  ElevatedButton(
                      style: ElevatedButton.styleFrom(),
                      onPressed: () async {
                        context.read<ListCubit>().add(await Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      AddGrocery(tagCount: getTags(state))),
                            ));
                        setState(() {});
                      },
                      child: const Text(
                        'Add item',
                        style: TextStyle(fontSize: 28),
                      )),
                ],
              );
            })));
  }

  List<Widget> getTextWidgets(List<GroceryItem> groceries) {
    List<Widget> list = [];
    for (var element in groceries) {
      list.add(Center(
          child: Text(
        element.name,
        style: const TextStyle(fontSize: 30),
      )));
    }
    return list;
  }

  int getPrice(List<GroceryItem> groceries) {
    int price = 0;
    for (var element in groceries) {
      price += element.price;
    }
    return price;
  }

  List<String> getTags(List<GroceryItem> items) {
    List<String> res = [];
    for (var x in items) {
      res.addAll(x.tags);
    }
    return res;
  }
}

//Clase auxiliar que guarda y actualiza información de manera sencilla//
class ListCubit extends Cubit<List<GroceryItem>> {
  ListCubit() : super([]);

  void add(GroceryItem groceryItem) {
    state.add(groceryItem);
    emit(state);
  }
}
