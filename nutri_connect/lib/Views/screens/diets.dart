import 'package:flutter/material.dart';
import 'package:nutri_connect/Services/database_service.dart';
import 'package:nutri_connect/Views/utils/color_find.dart';
import 'package:nutri_connect/locator.dart';
import 'package:nutri_connect/models/user_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Diets extends StatefulWidget {
  const Diets({super.key});

  @override
  State<Diets> createState() => DietsState();
}

class DietsState extends State<Diets> {
  DatabaseService service = DatabaseService();
  bool value1 = false;
  bool value2 = false;
  bool value3 = false;
  bool value4 = false;

  @override
  void initState() {
    _loadUserDietPreferences();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    setState(() {});
    return Scaffold(
      appBar: AppBar(
        title: const Text('Pick your diets!'),
        backgroundColor: hexStringToColor("60992D"),
      ),
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
            gradient: LinearGradient(colors: [
          hexStringToColor("60992D"),
          hexStringToColor("8CAE68"),
          hexStringToColor("DC851F")
        ], begin: Alignment.topCenter, end: Alignment.bottomCenter)),
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.fromLTRB(
                20,
                MediaQuery.of(context).size.height * 0.05,
                20,
                MediaQuery.of(context).size.height * 0.05),
            child: Column(
              children: <Widget>[
                const SizedBox(
                  height: 30,
                ),
                CheckboxListTile(
                  value: value1,
                  title: const Text(
                    'Vegan',
                    style: TextStyle(fontSize: 20),
                  ),
                  onChanged: _handleValue1,
                ),
                CheckboxListTile(
                  value: value2,
                  title: const Text('Veggie', style: TextStyle(fontSize: 20)),
                  onChanged: _handleValue2,
                ),
                CheckboxListTile(
                  value: value3,
                  title: const Text(
                    'Paleo',
                    style: TextStyle(fontSize: 20),
                  ),
                  onChanged: _handleValue3,
                ),
                CheckboxListTile(
                  value: value4,
                  title: const Text('Keto', style: TextStyle(fontSize: 20)),
                  onChanged: _handleValue4,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _handleValue1(bool? value) {
    if (value != null) {
      value1 = value;
      if (locator.get<UserModel>().diets.contains('Vegan')) {
        locator.get<UserModel>().removeDiets("Vegan");
        setState(() {
          value1 = false;
        });
      } else {
        locator.get<UserModel>().addDiets("Vegan");
        setState(() {
          value1 = true;
        });
      }
      locator
          .get<DatabaseService>()
          .changeInfo('diets', locator.get<UserModel>().diets);
      SharedPreferences.getInstance().then(
        (prefs) {
          prefs.setBool("Vegan", value);
        },
      );
    }
  }

  void _handleValue2(bool? value) {
    if (value != null) {
      value2 = value;
      if (locator.get<UserModel>().diets.contains('Veggie')) {
        locator.get<UserModel>().removeDiets("Veggie");
        setState(() {
          value2 = false;
        });
      } else {
        locator.get<UserModel>().addDiets("Veggie");
        setState(() {
          value2 = true;
        });
      }
      locator
          .get<DatabaseService>()
          .changeInfo('diets', locator.get<UserModel>().diets);
      SharedPreferences.getInstance().then(
        (prefs) {
          prefs.setBool("Veggie", value2);
        },
      );
    }
  }

  void _handleValue3(bool? value) {
    if (value != null) {
      value3 = value;
      if (locator.get<UserModel>().diets.contains('Paleo')) {
        locator.get<UserModel>().removeDiets("Paleo");
        setState(() {
          value3 = false;
        });
      } else {
        locator.get<UserModel>().addDiets("Paleo");
        setState(() {
          value3 = true;
        });
      }
      locator
          .get<DatabaseService>()
          .changeInfo('diets', locator.get<UserModel>().diets);
      SharedPreferences.getInstance().then(
        (prefs) {
          prefs.setBool("Paleo", value);
        },
      );
    }
  }

  void _handleValue4(bool? value) {
    if (value != null) {
      value4 = value;
      if (locator.get<UserModel>().diets.contains('Keto')) {
        locator.get<UserModel>().removeDiets("Keto");
        setState(() {
          value4 = false;
        });
      } else {
        locator.get<UserModel>().addDiets("Keto");
        setState(() {
          value4 = true;
        });
      }
      locator
          .get<DatabaseService>()
          .changeInfo('diets', locator.get<UserModel>().diets);
      SharedPreferences.getInstance().then(
        (prefs) {
          prefs.setBool("Keto", value);
        },
      );
    }
  }

  void _loadUserDietPreferences() async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      var value1 = prefs.getBool("Vegan") ?? false;
      var value2 = prefs.getBool("Veggie") ?? false;
      var value3 = prefs.getBool("Paleo") ?? false;
      var value4 = prefs.getBool("Keto") ?? false;
      setState(() {
        value1 = value1;
        value2 = value2;
        value3 = value3;
        value4 = value4;
      });
    } catch (e) {
      return;
    }
  }
}
