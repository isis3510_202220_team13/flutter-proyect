import 'package:flutter/material.dart';

import '../reusable_widgets/reusable.dart';
import '../utils/color_find.dart';

class LoadingScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
            gradient: LinearGradient(colors: [
          hexStringToColor("60992D"),
          hexStringToColor("8CAE68"),
          hexStringToColor("DC851F")
        ], begin: Alignment.topCenter, end: Alignment.bottomCenter)),
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.fromLTRB(
                20,
                MediaQuery.of(context).size.height * 0.05,
                20,
                MediaQuery.of(context).size.height * 0.05),
            child: Column(
              children: <Widget>[
                logoWidget("assets/images/logopeque.jpeg"),
                const SizedBox(
                  height: 30,
                ),
                circularProgress()
              ],
            ),
          ),
        ),
      ),
    );
  }
}
