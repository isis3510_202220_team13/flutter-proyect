import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../models/Grocery_item.dart';
import '../utils/color_find.dart';

class AddGrocery extends StatelessWidget {
  String name = "";
  int price = 0;
  String tagText = "";

  final List<String> tagCount;

  final List<String> tags = [];
  final EdgeInsets edges = const EdgeInsets.all(8.0);

  AddGrocery({super.key, required this.tagCount});

  Iterable<GroceryItem> getRecommendations() {
    List<GroceryItem> recommended = [
      GroceryItem(
          name: "Chips", price: 5, tags: ["sodium", "flour", "party", "fun"]),
      GroceryItem(
          name: "Doritos", price: 5, tags: ["sodium", "flour", "party", "fun"]),
      GroceryItem(
          name: "Soda", price: 5, tags: ["liquid", "sugar", "party", "fun"]),
      GroceryItem(
          name: "Beer", price: 5, tags: ["alcohol", "liquid", "party", "sad"]),
      GroceryItem(
          name: "Onions", price: 5, tags: ["vegetable", "meal", "ingredient"]),
    ];

    return recommended.where((element) {
      bool x = false;
      for (var value in element.tags) {
        x = x || tagCount.contains(value.toLowerCase());
      }
      return x;
    });
  }

  void back(BuildContext context, GroceryItem gi) {
    Navigator.pop(context, gi);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
            title: const Text('Add grocery'),
            backgroundColor: hexStringToColor("60992D")),
        body: ListView(
          children: [
            Padding(
                padding: edges,
                child: TextField(
                  inputFormatters: [
                    LengthLimitingTextInputFormatter(20),
                  ],
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Name',
                  ),
                  onChanged: (text) {
                    name = text;
                  },
                )),
            Padding(
                padding: edges,
                child: TextField(
                    inputFormatters: [
                      LengthLimitingTextInputFormatter(8),
                    ],
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Price',
                    ),
                    onChanged: (text) {
                      price = int.parse(text);
                    },
                    keyboardType: TextInputType.number)),
            const Padding(
                padding: EdgeInsets.all(8.0),
                child: Text('Enter tags separated by space:')),
            Padding(
                padding: edges,
                child: TextField(
                    inputFormatters: [
                      LengthLimitingTextInputFormatter(50),
                    ],
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Tags',
                    ),
                    onChanged: (text) {
                      tagText = text;
                    })),
            ElevatedButton(
                style: ElevatedButton.styleFrom(backgroundColor: hexStringToColor("60992D")),
                onPressed: () async {
                  tags.addAll(
                      tagText.split(' ').where((element) => element != ""));
                  if (name != '' && price != 0) {
                    back(context,
                        GroceryItem(name: name, price: price, tags: tags));
                  }
                },
                child: const Text(
                  'Add item',
                  style: TextStyle(fontSize: 28),
                )),
            Center(
                child: ListView(
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    children: [
                  for (var value in getRecommendations())
                    ElevatedButton(
                        onPressed: () async {
                          back(context, value);
                        },
                        child: Text(
                          value.name,
                          style: const TextStyle(fontSize: 28),
                        ))
                ])),
          ],
        ));
  }
}
