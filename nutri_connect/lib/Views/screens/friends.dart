import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nutri_connect/Services/database_service.dart';
import 'package:nutri_connect/Views/reusable_widgets/reusable.dart';
import 'package:nutri_connect/Views/screens/friend_search.dart';
import 'package:nutri_connect/Views/tabs/friend_profile.dart';
import 'package:nutri_connect/Views/utils/color_find.dart';
import 'package:nutri_connect/controllers/log_in_controller.dart';
import '../../locator.dart';
import '../../models/user_model.dart';

class Friends extends StatefulWidget {
  const Friends({super.key});

  @override
  State<Friends> createState() => FriendsState();
}

class FriendsState extends State<Friends> {
  int index = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Friends'),
          backgroundColor: hexStringToColor("60992D"),
        ),
        body: BlocProvider(
            //Crea el Cubit, el estado
            create: (_) => ListCubit(),
            child: BlocBuilder<ListCubit, List<String>>(
                //Refresca el componente para mostrar la información en cambio de estado
                buildWhen: (previousState, state) {
              return true;
            }, builder: (context, state) {
              return Column(
                children: [
                  Center(
                      child: ListView(
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    children: getTextWidget(state),
                  )),
                  ElevatedButton(
                      style: ElevatedButton.styleFrom(),
                      onPressed: () async {
                        bool b = await checkUserConnection();
                        if (!b) {
                          SnackBar bar = reusableSnackbar(
                              AuthExceptionHandler.generateExceptionMessage(
                                  AuthResultStatus.noConnection));
                          ScaffoldMessenger.of(context).showSnackBar(bar);
                        } else {
                          Navigator.pop(context);
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const FriendSearch()));
                        }
                      },
                      child: const Text(
                        'Search for friends',
                        style: TextStyle(fontSize: 28),
                      )),
                ],
              );
            })));
  }

  List<ElevatedButton> getTextWidget(List<String> state) {
    return state
        .map((e) => ElevatedButton(
            onPressed: () async {
              var email = e.split(':')[1];
              bool b = await checkUserConnection();
              if (!b) {
                SnackBar bar = reusableSnackbar(
                    AuthExceptionHandler.generateExceptionMessage(
                        AuthResultStatus.noConnection));
                ScaffoldMessenger.of(context).showSnackBar(bar);
              } else {
                var data = await DatabaseService().getUserComplete(email);
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => FriendTab(email: data!)));
              }
            },
            style: ElevatedButton.styleFrom(
                backgroundColor: Colors.white, foregroundColor: Colors.black87),
            child: Text(
              format(e),
              style: const TextStyle(fontSize: 20),
            )))
        .toList();
  }

  String format(String inp) {
    return inp.split(':')[0];
  }
}

//Clase auxiliar que guarda y actualiza información de manera sencilla//
class ListCubit extends Cubit<List<String>> {
  ListCubit() : super(locator.get<UserModel>().friends);

  void add(String friend) {
    state.add(friend);
    emit(state);
  }
}
