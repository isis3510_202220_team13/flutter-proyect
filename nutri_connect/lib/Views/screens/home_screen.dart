// Copyright 2019 The Flutter team. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nutri_connect/Views/tabs/profile_tab.dart';
import 'package:nutri_connect/controllers/log_in_controller.dart';

import '../../locator.dart';
import '../tabs/chatbot_tab.dart';
import '../tabs/scanner_tab.dart';
import '../tabs/list_tab.dart';

class _TabInfo {
  const _TabInfo(this.title, this.icon);

  final String title;
  final IconData icon;
}

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() {
    return _HomeScreenState();
  }
}

class _HomeScreenState extends State<HomeScreen> {
  List<Widget> data = [
    const ProfileTab(),
    const ScannerScreenTab(),
    const ListScreenTab(),
    const ChatbotScreenTab()
  ];

  @override
  Widget build(BuildContext context) {
    final tabInfo = [
      const _TabInfo(
        'Profile',
        CupertinoIcons.profile_circled,
      ),
      const _TabInfo(
        'Scanner',
        CupertinoIcons.barcode,
      ),
      const _TabInfo('Grocery list', CupertinoIcons.list_bullet),
      const _TabInfo('Chatbot', CupertinoIcons.chat_bubble)
    ];

    return WillPopScope(
      child: DefaultTextStyle(
        style: CupertinoTheme.of(context).textTheme.textStyle,
        child: CupertinoTabScaffold(
          restorationId: 'cupertino_tab_scaffold',
          tabBar: CupertinoTabBar(
            items: [
              for (final tabInfo in tabInfo)
                BottomNavigationBarItem(
                  label: tabInfo.title,
                  icon: Icon(tabInfo.icon),
                ),
            ],
          ),
          tabBuilder: (context, index) {
            return CupertinoTabView(
              builder: (context) {
                return data[index];
              },
            );
          },
        ),
      ),
      onWillPop: () async {
        final value = await showDialog<bool>(
            context: context,
            builder: (cont) => AlertDialog(
                    title: const Text("Warning"),
                    content: const Text(
                        "This will log you out and close the app. Do you want to continue?"),
                    actions: [
                      TextButton(
                          child: const Text("No"),
                          onPressed: () {
                            Navigator.pop(cont);
                          }),
                      TextButton(
                          child: const Text("Exit"),
                          onPressed: () {
                            Navigator.pop(cont);
                            locator.get<FirebaseAuthHelper>().logout();
                            exit(0);
                          }),
                    ]));
        if (value != null) {
          return Future.value(value);
        } else {
          return Future.value(false);
        }
      },
    );
  }
}
