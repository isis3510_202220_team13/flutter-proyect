import 'package:flutter/material.dart';
import 'package:dialog_flowtter/dialog_flowtter.dart';
import 'package:nutri_connect/Views/reusable_widgets/reusable.dart';
import 'package:nutri_connect/Views/utils/color_find.dart';
import 'package:nutri_connect/controllers/log_in_controller.dart';

class ChatBotHome extends StatefulWidget {
  @override
  _ChatBotHomeState createState() => _ChatBotHomeState();
}

class _ChatBotHomeState extends State<ChatBotHome> {
  final TextEditingController _controller = TextEditingController();

  late DialogFlowtter _dialogFlowtter;

  @override
  void initState() {
    super.initState();
    DialogAuthCredentials.fromFile("assets/auth.json")
        .then((value) => _dialogFlowtter = DialogFlowtter(credentials: value));
  }

  List<Map<String, dynamic>> messages = [];

  void sendMessage(String text) async {
    if (text.isEmpty) return;
    setState(() {
      Message userMessage = Message(text: DialogText(text: [text]));
      addMessage(userMessage, true);
    });

    bool b = await checkUserConnection();
    if (!b) {
      SnackBar bar = reusableSnackbar(
          AuthExceptionHandler.generateExceptionMessage(
              AuthResultStatus.noConnection));
      ScaffoldMessenger.of(context).showSnackBar(bar);
      return;
    }

    QueryInput query = QueryInput(text: TextInput(text: text));

    DetectIntentResponse res = await _dialogFlowtter.detectIntent(
      queryInput: query,
    );

    if (res.message == null) return;
    Message m = res.message!;
    setState(() {
      addMessage(m);
    });
  }

  void addMessage(Message message, [bool isUserMessage = false]) {
    messages.add({
      'message': message,
      'isUserMessage': isUserMessage,
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          title: const Text('Chatbot Dialogflow Integration'),
          backgroundColor: hexStringToColor("60992D"),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.72,
                child: _MessagesList(messages: messages),
              ),
              Container(
                height: MediaQuery.of(context).size.height * 0.10,
                padding:
                    const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                color: Colors.blue,
                child: Row(
                  children: [
                    Expanded(
                      child: TextField(
                        style: const TextStyle(color: Colors.white),
                        controller: _controller,
                        cursorColor: Colors.white,
                      ),
                    ),
                    IconButton(
                      color: Colors.white,
                      icon: const Icon(Icons.send),
                      onPressed: () {
                        sendMessage(_controller.text);
                        _controller.clear();
                      },
                    ),
                  ],
                ),
              ),
            ],
          ),
        ));
  }
}

class _MessagesList extends StatelessWidget {
  final List<Map<String, dynamic>> messages;

  const _MessagesList({
    this.messages = const [],
  });

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      itemCount: messages.length,

      // Agregamos espaciado
      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 20),

      separatorBuilder: (_, i) => const SizedBox(height: 10),
      itemBuilder: (context, i) {
        var obj = messages[messages.length - 1 - i];
        return _MessageContainer(
          message: obj['message'],
          isUserMessage: obj['isUserMessage'],
        );
      },

      reverse: true,
    );
  }
}

class _MessageContainer extends StatelessWidget {
  final Message message;
  final bool isUserMessage;

  const _MessageContainer({
    required this.message,
    this.isUserMessage = false,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment:
          isUserMessage ? MainAxisAlignment.end : MainAxisAlignment.start,
      children: [
        Container(
          constraints: const BoxConstraints(maxWidth: 250),
          child: Container(
            decoration: BoxDecoration(
              color: isUserMessage ? Colors.blue : Colors.orange,
              borderRadius: BorderRadius.circular(20),
            ),
            padding: const EdgeInsets.all(10),
            child: Text(
              message.text?.text?[0] ?? '',
              style: const TextStyle(
                color: Colors.white,
              ),
            ),
          ),
        ),
      ],
    );
  }
}

class _CardContainer extends StatelessWidget {
  final DialogCard card;

  const _CardContainer({
    required this.card,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.orange,
      child: Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            constraints: const BoxConstraints.expand(height: 150),
            child: Image.network(
              card.imageUri!,
              fit: BoxFit.cover,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  card.title!,
                  style: const TextStyle(
                    fontSize: 22,
                    color: Colors.white,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                if (card.subtitle != null)
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10),
                    child: Text(
                      card.subtitle!,
                      style: const TextStyle(color: Colors.white),
                    ),
                  ),
                if ((card.buttons?.length)! > 0)
                  Container(
                    constraints: const BoxConstraints(
                      maxHeight: 40,
                    ),
                    child: ListView.separated(
                      shrinkWrap: true,
                      scrollDirection: Axis.horizontal,
                      // padding: const EdgeInsets.symmetric(vertical: 5),
                      itemBuilder: (context, i) {
                        CardButton button = card.buttons![i];
                        return ElevatedButton(
                          style: ButtonStyle(
                              backgroundColor:
                                  MaterialStateProperty.all(Colors.blue),
                              textStyle: MaterialStateProperty.all(
                                  const TextStyle(color: Colors.white))),
                          child: Text(button.text!),
                          onPressed: () {
                            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                              content: Text(button.postback!),
                            ));
                          },
                        );
                      },
                      separatorBuilder: (_, i) => Container(width: 10),
                      itemCount: card.buttons!.length,
                    ),
                  ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
