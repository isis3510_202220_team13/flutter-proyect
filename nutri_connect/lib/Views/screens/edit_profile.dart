import 'package:flutter/material.dart';
import 'package:nutri_connect/Views/reusable_widgets/reusable.dart';
import 'package:nutri_connect/Views/screens/loading_green.dart';
import 'package:nutri_connect/Views/utils/color_find.dart';
import 'package:nutri_connect/controllers/profile_controller.dart';
import 'package:nutri_connect/locator.dart';
import 'package:nutri_connect/models/user_model.dart';

class EditProfilePage extends StatefulWidget {
  @override
  _EditProfilePageState createState() => _EditProfilePageState();
}

class _EditProfilePageState extends State<EditProfilePage> {
  final TextEditingController _usernameController = TextEditingController();
  bool _isLoading = false;

  @override
  Widget build(BuildContext context) => _isLoading
      ? LoadingGreen()
      : Scaffold(
          appBar: AppBar(
            title: const Text('Edit profile'),
            backgroundColor: hexStringToColor("60992D"),
          ),
          backgroundColor: hexStringToColor("60992D"),
          body: SafeArea(
              minimum: const EdgeInsets.only(top: 50),
              child: ListView(
                  padding: const EdgeInsets.symmetric(horizontal: 32),
                  physics: const BouncingScrollPhysics(),
                  children: [
                    ProfileWidget(
                        imagePath: locator.get<UserModel>().giveImagePath(),
                        isEdit: true,
                        onclicked: () async {
                          showDialog(
                              context: context,
                              builder: (cont) => AlertDialog(
                                      title:
                                          const Text("Change profile picture"),
                                      content: const Text(
                                          "Would you like to pick a picture from your gallery or from your camera?"),
                                      actions: [
                                        TextButton(
                                            child: const Text('Gallery'),
                                            onPressed: () async {
                                              setState(() => _isLoading = true);
                                              Navigator.pop(cont);
                                              await locator
                                                  .get<ProfileController>()
                                                  .pickUploadImage();
                                              setState(
                                                  () => _isLoading = false);
                                            }),
                                        TextButton(
                                            child: const Text('Camera'),
                                            onPressed: () async {
                                              setState(() => _isLoading = true);
                                              Navigator.pop(cont);
                                              await locator
                                                  .get<ProfileController>()
                                                  .takeUploadImage();
                                              setState(
                                                  () => _isLoading = false);
                                            }),
                                        TextButton(
                                            child: const Text('Cancel'),
                                            onPressed: () {
                                              Navigator.pop(cont);
                                            }),
                                      ]));
                        }),
                    const SizedBox(height: 24),
                    const Text(
                      'Change username',
                      style: TextStyle(
                        fontSize: 20.0,
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontFamily: "Pacifico",
                      ),
                    ),
                    const SizedBox(height: 12),
                    reusableTextField(locator.get<UserModel>().name,
                        Icons.person_outline, false, 40, _usernameController),
                    firebaseUIButton(context, "Change it!", () async {
                      await locator.get<ProfileController>().changeUsername(
                          context,
                          locator.get<UserModel>().name,
                          _usernameController.text);
                    })
                  ])));
}
