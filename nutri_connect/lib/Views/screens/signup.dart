import 'package:flutter/material.dart';
import '../../controllers/log_in_controller.dart';
import '../../locator.dart';
import '../reusable_widgets/reusable.dart';
import '../utils/color_find.dart';
import 'loading_screen.dart';

class SignUpScreen extends StatefulWidget {
  const SignUpScreen({Key? key}) : super(key: key);

  @override
  // ignore: library_private_types_in_public_api
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  final TextEditingController _passwordTextController = TextEditingController();
  final TextEditingController _emailTextController = TextEditingController();
  final TextEditingController _userNameTextController = TextEditingController();
  bool _isLoading = false;

  AuthExceptionHandler handler = AuthExceptionHandler();
  @override
  Widget build(BuildContext context) => _isLoading
      ? LoadingScreen()
      : Scaffold(
          extendBodyBehindAppBar: true,
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0,
            title: const Text(
              "Sign Up",
              style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
            ),
          ),
          body: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              decoration: BoxDecoration(
                  gradient: LinearGradient(colors: [
                hexStringToColor("60992D"),
                hexStringToColor("8CAE68"),
                hexStringToColor("DC851F")
              ], begin: Alignment.topCenter, end: Alignment.bottomCenter)),
              child: SingleChildScrollView(
                  child: Padding(
                padding: const EdgeInsets.fromLTRB(20, 120, 20, 0),
                child: Column(
                  children: <Widget>[
                    const SizedBox(
                      height: 20,
                    ),
                    reusableTextField("Enter User Name", Icons.person_outline,
                        false, 40, _userNameTextController),
                    const SizedBox(
                      height: 20,
                    ),
                    reusableTextField("Enter Email", Icons.person_outline,
                        false, 40, _emailTextController),
                    const SizedBox(
                      height: 20,
                    ),
                    reusableTextField("Enter Password", Icons.lock_outlined,
                        true, 20, _passwordTextController),
                    const SizedBox(
                      height: 20,
                    ),
                    firebaseUIButton(context, "Sign Up", () async {
                      setState(() => _isLoading = true);
                      await locator.get<FirebaseAuthHelper>().createAccount(
                          user: _userNameTextController.text,
                          email: _emailTextController.text,
                          pass: _passwordTextController.text,
                          cont: context);
                      setState(() => _isLoading = false);
                    })
                  ],
                ),
              ))),
        );
}
