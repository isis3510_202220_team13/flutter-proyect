import 'package:flutter/material.dart';
import 'package:nutri_connect/Services/database_service.dart';
import 'package:nutri_connect/Views/reusable_widgets/reusable.dart';
import 'package:nutri_connect/Views/utils/color_find.dart';

import 'loading_screen.dart';

class FriendSearch extends StatefulWidget {
  const FriendSearch({super.key});

  @override
  State<FriendSearch> createState() => FriendSearchState();
}

class FriendSearchState extends State<FriendSearch> {
  final TextEditingController _friendSearchController = TextEditingController();
  DatabaseService service = DatabaseService();
  bool _isLoading = false;

  @override
  Widget build(BuildContext context) => _isLoading
      ? LoadingScreen()
      : Scaffold(
          appBar: AppBar(
            title: const Text('Find new friends'),
            backgroundColor: hexStringToColor("60992D"),
          ),
          body: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
                gradient: LinearGradient(colors: [
              hexStringToColor("60992D"),
              hexStringToColor("8CAE68"),
              hexStringToColor("DC851F")
            ], begin: Alignment.topCenter, end: Alignment.bottomCenter)),
            child: SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.fromLTRB(
                    20,
                    MediaQuery.of(context).size.height * 0.05,
                    20,
                    MediaQuery.of(context).size.height * 0.05),
                child: Column(
                  children: <Widget>[
                    const SizedBox(
                      height: 30,
                    ),
                    reusableTextField(
                        "Enter friend's email here",
                        Icons.person_outline,
                        false,
                        40,
                        _friendSearchController),
                    const SizedBox(
                      height: 20,
                    ),
                    firebaseUIButton(context, "Search!", () async {
                      setState(() => _isLoading = true);
                      await service.getUserForFind(
                          context, _friendSearchController.text);
                      setState(() => _isLoading = false);
                    }),
                    const SizedBox(
                      height: 5,
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
}
