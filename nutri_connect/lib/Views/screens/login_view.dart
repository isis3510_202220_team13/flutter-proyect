import 'package:flutter/material.dart';
import 'package:nutri_connect/Views/screens/loading_screen.dart';
import 'package:nutri_connect/Views/screens/reset_password.dart';
import 'package:nutri_connect/Views/screens/signup.dart';
import 'package:nutri_connect/controllers/log_in_controller.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../locator.dart';
import '../reusable_widgets/reusable.dart';
import '../utils/color_find.dart';

class SignInScreen extends StatefulWidget {
  const SignInScreen({Key? key}) : super(key: key);

  @override
  _SignInScreenState createState() => _SignInScreenState();
}

class _SignInScreenState extends State<SignInScreen> {
  final TextEditingController _passwordTextController = TextEditingController();
  final TextEditingController _emailTextController = TextEditingController();
  bool _isChecked = false;
  bool _isLoading = false;

  @override
  void initState() {
    _loadUserEmailPassword();
    super.initState();
  }

  AuthExceptionHandler handler = AuthExceptionHandler();

  @override
  Widget build(BuildContext context) => _isLoading
      ? LoadingScreen()
      : Scaffold(
          body: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
                gradient: LinearGradient(colors: [
              hexStringToColor("60992D"),
              hexStringToColor("8CAE68"),
              hexStringToColor("DC851F")
            ], begin: Alignment.topCenter, end: Alignment.bottomCenter)),
            child: SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.fromLTRB(
                    20,
                    MediaQuery.of(context).size.height * 0.05,
                    20,
                    MediaQuery.of(context).size.height * 0.05),
                child: Column(
                  children: <Widget>[
                    logoWidget("assets/images/logopeque.jpeg"),
                    const SizedBox(
                      height: 30,
                    ),
                    reusableTextField("Enter Email", Icons.person_outline,
                        false, 40, _emailTextController),
                    const SizedBox(
                      height: 20,
                    ),
                    reusableTextField("Enter Password", Icons.lock_outline,
                        true, 20, _passwordTextController),
                    const SizedBox(
                      height: 5,
                    ),
                    rememberMe(context),
                    forgetPassword(context),
                    firebaseUIButton(context, "Sign In", () async {
                      setState(() => _isLoading = true);
                      await locator.get<FirebaseAuthHelper>().login(
                          email: _emailTextController.text,
                          pass: _passwordTextController.text,
                          cont: context);
                      setState(() => _isLoading = false);

                      _handleRememberMe(_isChecked);
                    }),
                    signUpOption()
                  ],
                ),
              ),
            ),
          ),
        );

  Row signUpOption() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const Text("Don't have account?",
            style: TextStyle(color: Colors.white70)),
        GestureDetector(
          onTap: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => const SignUpScreen()));
          },
          child: const Text(
            " Sign Up",
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
          ),
        )
      ],
    );
  }

  Widget forgetPassword(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 35,
      alignment: Alignment.bottomRight,
      child: TextButton(
        child: const Text(
          "Forgot Password?",
          style: TextStyle(color: Colors.white70),
          textAlign: TextAlign.right,
        ),
        onPressed: () => Navigator.push(context,
            MaterialPageRoute(builder: (context) => const ResetPassword())),
      ),
    );
  }

  Widget rememberMe(BuildContext context) {
    return Row(
      children: [
        Checkbox(value: _isChecked, onChanged: _handleRememberMe),
        const Text(
          "Remember me",
          style: TextStyle(color: Colors.white70),
        )
      ],
    );
  }

  void _handleRememberMe(bool? value) {
    if (value != null) {
      _isChecked = value;
      SharedPreferences.getInstance().then(
        (prefs) {
          prefs.setBool("remember_me", value);
          prefs.setString('email', _emailTextController.text);
          prefs.setString('password', _passwordTextController.text);
        },
      );
      setState(() {
        _isChecked = value;
      });
    }
  }

  void _loadUserEmailPassword() async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      var email = prefs.getString("email") ?? "";
      var password = prefs.getString("password") ?? "";
      var remeberMe = prefs.getBool("remember_me") ?? false;
      if (remeberMe) {
        setState(() {
          _isChecked = true;
        });
        _emailTextController.text = email;
        _passwordTextController.text = password;
      }
    } catch (e) {
      return;
    }
  }
}
