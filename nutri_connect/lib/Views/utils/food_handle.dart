import 'dart:async';

import 'package:openfoodfacts/model/OcrIngredientsResult.dart';
import 'package:openfoodfacts/openfoodfacts.dart';
import 'package:openfoodfacts/utils/TagType.dart';

/// request a product from the OpenFoodFacts database
Future<Product?> getProduct(var barcode) async {
  ProductQueryConfiguration configuration = ProductQueryConfiguration(barcode,
      language: OpenFoodFactsLanguage.GERMAN, fields: [ProductField.ALL]);
  ProductResult result = await OpenFoodAPIClient.getProduct(configuration);

  if (result.status == 1) {
    return result.product;
  } else {
    throw Exception('product not found, please insert data for $barcode');
  }
}

/// add a new product to the OpenFoodFacts database
void addNewProduct(var barcode1, var productName1) async {
  // define the product to be added.
  // more attributes available ...
  Product myProduct = Product(
    barcode: barcode1,
    productName: productName1,
  );

  // a registered user login for https://world.openfoodfacts.org/ is required
  User myUser = const User(
      userId: 'ja.carvajalm@uniandes.edu.co',
      password: 'AplicacionesMovilesG13');

  // query the OpenFoodFacts API
  Status result = await OpenFoodAPIClient.saveProduct(myUser, myProduct);

  if (result.status != 1) {
    throw Exception('product could not be added: ${result.error}');
  }
}

/// add a new image for an existing product of the OpenFoodFacts database
void addProductImage(var bc, var path) async {
  // define the product image
  // set the uri to the local image file
  // choose the "imageField" as location / description of the image content.
  SendImage image = SendImage(
    lang: OpenFoodFactsLanguage.ENGLISH,
    barcode: bc,
    imageField: ImageField.INGREDIENTS,
    imageUri: Uri.parse(path),
  );

  // a registered user login for https://world.openfoodfacts.org/ is required
  User myUser = const User(
      userId: 'ja.carvajalm@uniandes.edu.co',
      password: 'AplicacionesMovilesG13');

  // query the OpenFoodFacts API
  Status result = await OpenFoodAPIClient.addProductImage(myUser, image);

  if (result.status != 'status ok') {
    throw Exception(
        'image could not be uploaded: ${result.error} ${result.imageId.toString()}');
  }
}

/// Extract the ingredients of an existing product of the OpenFoodFacts database
/// That has already ingredient image
/// Otherwise it should be added first to the server and then this can be called
Future<String?> extractIngredient(var bc) async {
  // a registered user login for https://world.openfoodfacts.org/ is required
  User myUser = const User(
      userId: 'ja.carvajalm@uniandes.edu.co',
      password: 'AplicacionesMovilesG13');

  // query the OpenFoodFacts API
  OcrIngredientsResult response = await OpenFoodAPIClient.extractIngredients(
      myUser, bc, OpenFoodFactsLanguage.ENGLISH);

  if (response.status != 0) {
    throw Exception("Text can't be extracted.");
  }
  return response.ingredientsTextFromImage;
}

/// Extract the ingredients of an existing product of the OpenFoodFacts database
/// That does not have ingredient image
/// And then save it back to the OFF server
void saveAndExtractIngredient(var bc, var path) async {
  // a registered user login for https://world.openfoodfacts.org/ is required
  User myUser = const User(
      userId: 'ja.carvajalm@uniandes.edu.co',
      password: 'AplicacionesMovilesG13');

  SendImage image = SendImage(
    lang: OpenFoodFactsLanguage.ENGLISH,
    barcode: bc,
    imageField: ImageField.INGREDIENTS,
    imageUri: Uri.parse(path),
  );

  //Add the ingredients image to the server
  Status results = await OpenFoodAPIClient.addProductImage(myUser, image);

  if (results.status == null) {
    throw Exception('Adding image failed');
  }

  OcrIngredientsResult ocrResponse = await OpenFoodAPIClient.extractIngredients(
      myUser, bc, OpenFoodFactsLanguage.ENGLISH);

  if (ocrResponse.status != 0) {
    throw Exception("Text can't be extracted.");
  }

  // Save the extracted ingredients to the product on the OFF server
  results = await OpenFoodAPIClient.saveProduct(
      myUser,
      Product(
          barcode: bc, ingredientsText: ocrResponse.ingredientsTextFromImage));

  if (results.status != 1) {
    throw Exception('product could not be added');
  }

  //Get The saved product's ingredients from the server
  ProductQueryConfiguration configurations = ProductQueryConfiguration(bc,
      language: OpenFoodFactsLanguage.ENGLISH,
      fields: [
        ProductField.INGREDIENTS_TEXT,
      ]);
  ProductResult productResult =
      await OpenFoodAPIClient.getProduct(configurations, user: myUser);

  if (productResult.status != 1) {
    throw Exception('product not found, please insert data for 3613042717385');
  }
}

/// Get suggestion based on:
/// Your user input
/// The preference language
/// The TagType
void getSuggestions() async {
  // The result will be a List<dynamic> that can be parsed
  await OpenFoodAPIClient.getAutocompletedSuggestions(TagType.COUNTRIES,
      input: 'Tun', language: OpenFoodFactsLanguage.ENGLISH);
}
