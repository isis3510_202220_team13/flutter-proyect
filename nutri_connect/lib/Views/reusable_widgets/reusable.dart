import 'package:flutter/material.dart';
import 'package:nutri_connect/Views/utils/color_find.dart';

Image logoWidget(String imageName) {
  return Image.asset(
    imageName,
    fit: BoxFit.fitWidth,
    width: 240,
    height: 240,
  );
}

Widget buildImage(imagePath) {
  if (imagePath != '') {
    final image = NetworkImage(imagePath);
    return ClipOval(
        child: Material(
            color: Colors.transparent,
            child: Ink.image(
                image: image,
                fit: BoxFit.cover,
                width: 100,
                height: 100,
                child: const InkWell(onTap: null))));
  } else {
    const image = NetworkImage(
        'https://firebasestorage.googleapis.com/v0/b/nutri-connected.appspot.com/o/User_IMAGE1670174990637.jpg?alt=media&token=c36db5ac-be12-4435-9567-e9c42629beec');
    return ClipOval(
        child: Material(
            color: Colors.transparent,
            child: Ink.image(
                image: image,
                fit: BoxFit.cover,
                width: 100,
                height: 100,
                child: const InkWell(onTap: null))));
  }
}

TextField reusableTextField(String text, IconData icon, bool isPasswordType,
    int textSize, TextEditingController controller) {
  return TextField(
    controller: controller,
    obscureText: isPasswordType,
    enableSuggestions: !isPasswordType,
    autocorrect: !isPasswordType,
    cursorColor: Colors.white,
    maxLength: textSize,
    style: TextStyle(color: Colors.white.withOpacity(0.9)),
    decoration: InputDecoration(
      prefixIcon: Icon(
        icon,
        color: Colors.white70,
      ),
      labelText: text,
      labelStyle: TextStyle(color: Colors.white.withOpacity(0.9)),
      filled: true,
      floatingLabelBehavior: FloatingLabelBehavior.never,
      fillColor: Colors.white.withOpacity(0.3),
      border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(30.0),
          borderSide: const BorderSide(width: 0, style: BorderStyle.none)),
    ),
    keyboardType: isPasswordType
        ? TextInputType.visiblePassword
        : TextInputType.emailAddress,
  );
}

Container firebaseUIButton(BuildContext context, String title, Function onTap) {
  return Container(
    width: MediaQuery.of(context).size.width,
    height: 50,
    margin: const EdgeInsets.fromLTRB(0, 10, 0, 20),
    decoration: BoxDecoration(borderRadius: BorderRadius.circular(90)),
    child: ElevatedButton(
      onPressed: () {
        onTap();
      },
      style: ButtonStyle(
          backgroundColor: MaterialStateProperty.resolveWith((states) {
            if (states.contains(MaterialState.pressed)) {
              return Colors.black26;
            }
            return Colors.white;
          }),
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)))),
      child: Text(
        title,
        style: const TextStyle(
            color: Colors.black87, fontWeight: FontWeight.bold, fontSize: 16),
      ),
    ),
  );
}

SnackBar reusableSnackbar(String cont) {
  return SnackBar(content: Text(cont));
}

Container circularProgress() {
  return Container(
    alignment: Alignment.center,
    padding: const EdgeInsets.only(top: 10),
    child: const CircularProgressIndicator(
        valueColor: AlwaysStoppedAnimation(Colors.white)),
  );
}

class InfoCard extends StatelessWidget {
  // the values we need
  final String text;
  final IconData icon;
  final Function() onPressed;

  const InfoCard(
      {super.key,
      required this.text,
      required this.icon,
      required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      child: Card(
        color: Colors.white,
        margin: const EdgeInsets.symmetric(vertical: 10, horizontal: 25),
        child: ListTile(
          leading: Icon(
            icon,
            color: Colors.teal,
          ),
          title: Text(
            text,
            style: const TextStyle(
                color: Colors.teal,
                fontSize: 20,
                fontFamily: "Source Sans Pro"),
          ),
        ),
      ),
    );
  }
}

class BigInfoCard extends StatelessWidget {
  // the values we need
  final String title;
  final IconData icon;
  final String text;
  final Function() onPressed;

  const BigInfoCard(
      {super.key,
      required this.title,
      required this.icon,
      required this.text,
      required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      child: Card(
        color: Colors.white,
        margin: const EdgeInsets.symmetric(vertical: 10, horizontal: 25),
        child: SizedBox(
            width: 350,
            height: 100,
            child: Column(
              children: <Widget>[
                ListTile(
                  leading: Icon(
                    icon,
                    color: Colors.teal,
                  ),
                  title: Text(
                    title,
                    style: const TextStyle(
                        color: Colors.teal,
                        fontSize: 20,
                        fontFamily: "Source Sans Pro"),
                  ),
                ),
                Text(text),
              ],
            )),
      ),
    );
  }
}

class ProfileWidget extends StatelessWidget {
  final String imagePath;
  final VoidCallback onclicked;
  final bool isEdit;

  const ProfileWidget({
    Key? key,
    required this.imagePath,
    required this.isEdit,
    required this.onclicked,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final color = Theme.of(context).colorScheme.primary;

    return Center(
        child: Stack(children: [
      buildImage(),
      Positioned(bottom: 0, right: 3, child: buildEditIcon(color))
    ]));
  }

  Widget buildImage() {
    final image = NetworkImage(imagePath);

    return ClipOval(
        child: Material(
            color: Colors.transparent,
            child: Ink.image(
                image: image,
                fit: BoxFit.cover,
                width: 100,
                height: 100,
                child: InkWell(onTap: onclicked))));
  }

  Widget buildEditIcon(Color color) => buildCircle(
      color: Colors.white,
      all: 3,
      child: buildCircle(
          color: hexStringToColor("60992D"),
          all: 5,
          child: Icon(
            isEdit ? Icons.add_a_photo : Icons.edit,
            color: Colors.white,
            size: 15,
          )));

  Widget buildCircle(
          {required Widget child, required double all, required Color color}) =>
      ClipOval(
          child: Container(
        padding: EdgeInsets.all(all),
        color: color,
        child: child,
      ));
}
