import 'package:get_it/get_it.dart';
import 'package:nutri_connect/Services/database_service.dart';
import 'package:nutri_connect/controllers/log_in_controller.dart';
import 'package:nutri_connect/controllers/profile_controller.dart';
import 'package:nutri_connect/controllers/storage_controller.dart';
import 'models/FoodInfo.dart';
import 'models/FriendStorage.dart';
import 'models/user_model.dart';

final locator = GetIt.instance;

void setup() {
  locator.registerSingleton<FirebaseAuthHelper>(FirebaseAuthHelper());
  locator.registerFactory<DatabaseService>(() => DatabaseService());
  locator.registerSingleton<UserModel>(UserModel(
      name: "test",
      diets: [""],
      email: "test@test.com",
      friends: [""],
      imagePath: ''));
  locator.registerSingleton<FoodStorage>(FoodStorage());
  locator.registerSingleton<FriendStorage>(FriendStorage());
  locator.registerSingleton<StorageController>(StorageController());
  locator.registerSingleton<ProfileController>(ProfileController());
}
