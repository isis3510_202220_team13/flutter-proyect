import 'dart:io';
import 'dart:isolate';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:nutri_connect/Services/database_service.dart';
import 'package:nutri_connect/Views/reusable_widgets/reusable.dart';
import 'package:nutri_connect/Views/screens/home_screen.dart';
import 'package:nutri_connect/controllers/storage_controller.dart';
import 'package:nutri_connect/locator.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../models/user_model.dart';

enum AuthResultStatus {
  successful,
  emailAlreadyExists,
  wrongPassword,
  invalidEmail,
  userNotFound,
  userDisabled,
  operationNotAllowed,
  tooManyRequests,
  noConnection,
  undefined,
}

class AuthExceptionHandler {
  static handleException(e) {
    var a = e.code;
    AuthResultStatus status;
    switch (a) {
      case "invalid-email":
        status = AuthResultStatus.invalidEmail;
        break;
      case "wrong-password":
        status = AuthResultStatus.wrongPassword;
        break;
      case "user-not-found":
        status = AuthResultStatus.userNotFound;
        break;
      case "ERROR_USER_DISABLED":
        status = AuthResultStatus.userDisabled;
        break;
      case "ERROR_TOO_MANY_REQUESTS":
        status = AuthResultStatus.tooManyRequests;
        break;
      case "ERROR_OPERATION_NOT_ALLOWED":
        status = AuthResultStatus.operationNotAllowed;
        break;
      case "email-already-in-use":
        status = AuthResultStatus.emailAlreadyExists;
        break;
      case "NO_CONNECTION":
        status = AuthResultStatus.noConnection;
        break;
      default:
        status = AuthResultStatus.undefined;
    }
    return status;
  }

  ///
  /// Accepts AuthExceptionHandler.errorType
  ///
  static generateExceptionMessage(exceptionCode) {
    String errorMessage;
    switch (exceptionCode) {
      case AuthResultStatus.invalidEmail:
        errorMessage = "Your email address appears to be malformed.";
        break;
      case AuthResultStatus.wrongPassword:
        errorMessage = "Your password is wrong.";
        break;
      case AuthResultStatus.userNotFound:
        errorMessage = "User with this email doesn't exist.";
        break;
      case AuthResultStatus.userDisabled:
        errorMessage = "User with this email has been disabled.";
        break;
      case AuthResultStatus.tooManyRequests:
        errorMessage = "Too many requests. Try again later.";
        break;
      case AuthResultStatus.operationNotAllowed:
        errorMessage = "Signing in with Email and Password is not enabled.";
        break;
      case AuthResultStatus.emailAlreadyExists:
        errorMessage =
            "The email has already been registered. Please login or reset your password.";
        break;
      case AuthResultStatus.noConnection:
        errorMessage = "Please check your internet connection and try again.";
        break;
      default:
        errorMessage = "An undefined Error happened.";
    }

    return errorMessage;
  }
}

Future checkUserConnection() async {
  ReceivePort port = ReceivePort();
  final isolate = await Isolate.spawn<List<dynamic>>(check, [port.sendPort]);
  final status = await port.first;
  isolate.kill(priority: Isolate.immediate);
  return status;
}

void check(List<dynamic> values) async {
  bool res = false;
  try {
    final result = await InternetAddress.lookup('google.com');
    if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
      res = true;
    }
  } on SocketException catch (_) {
    res = false;
  }
  SendPort sendPort = values[0];
  sendPort.send(res);
}

class FirebaseAuthHelper {
  final _auth = FirebaseAuth.instance;
  late AuthResultStatus _status;
  DatabaseService database = DatabaseService();

  Future<AuthResultStatus> createAccount(
      {user, email, pass, required BuildContext cont}) async {
    if (user == "" || email == "" || pass == "") {
      SnackBar bar = reusableSnackbar("Spaces cannot be left blank.");
      ScaffoldMessenger.of(cont).showSnackBar(bar);
    }
    if (pass.length < 6) {
      SnackBar bar = reusableSnackbar("Password is too short.");
      ScaffoldMessenger.of(cont).showSnackBar(bar);
    }
    bool b = await checkUserConnection();
    if (!b) {
      _status = AuthResultStatus.noConnection;
      SnackBar bar = reusableSnackbar(
          AuthExceptionHandler.generateExceptionMessage(
              AuthResultStatus.noConnection));
      ScaffoldMessenger.of(cont).showSnackBar(bar);

      return _status;
    }
    try {
      UserCredential authResult = await _auth.createUserWithEmailAndPassword(
          email: email, password: pass);
      if (authResult.user != null) {
        _status = AuthResultStatus.successful;
        database.addUser(
            name: user, diet: [], email: email, friends: [], image: '');
        locator.get<UserModel>().changeName(user);
        locator.get<UserModel>().changeEmail(email);
        Navigator.push(
            cont, MaterialPageRoute(builder: (context) => HomeScreen()));
      } else {
        _status = AuthResultStatus.undefined;
        SnackBar bar = reusableSnackbar(
            AuthExceptionHandler.generateExceptionMessage(_status));
        ScaffoldMessenger.of(cont).showSnackBar(bar);
      }
    } catch (e) {
      _status = AuthExceptionHandler.handleException(e);
      SnackBar bar = reusableSnackbar(
          AuthExceptionHandler.generateExceptionMessage(_status));
      ScaffoldMessenger.of(cont).showSnackBar(bar);
    }
    return _status;
  }

  Future<AuthResultStatus> login(
      {email, pass, required BuildContext cont}) async {
    if (email == "" || pass == "") {
      SnackBar bar = reusableSnackbar("Spaces cannot be left blank.");
      ScaffoldMessenger.of(cont).showSnackBar(bar);
    }

    bool b = await checkUserConnection();
    if (!b) {
      _status = AuthResultStatus.noConnection;
      SnackBar bar = reusableSnackbar(
          AuthExceptionHandler.generateExceptionMessage(
              AuthResultStatus.noConnection));
      ScaffoldMessenger.of(cont).showSnackBar(bar);

      return _status;
    }
    try {
      final authResult =
          await _auth.signInWithEmailAndPassword(email: email, password: pass);
      if (authResult.user != null) {
        _status = AuthResultStatus.successful;
        locator.get<UserModel>().changeEmail(email);
        await locator.get<UserModel>().refresh();
        await checkOfflineChanges();
        SharedPreferences.getInstance().then(
          (prefs) {
            for (int i = 0; i < locator.get<UserModel>().diets.length; i++) {
              prefs.setBool(locator.get<UserModel>().diets[i], true);
            }
          },
        );
        Navigator.push(
            cont, MaterialPageRoute(builder: (context) => HomeScreen()));
      } else {
        _status = AuthResultStatus.undefined;
        SnackBar bar = reusableSnackbar(
            AuthExceptionHandler.generateExceptionMessage(_status));
        ScaffoldMessenger.of(cont).showSnackBar(bar);
      }
    } catch (e) {
      _status = AuthExceptionHandler.handleException(e);
      SnackBar bar = reusableSnackbar(
          AuthExceptionHandler.generateExceptionMessage(_status));
      ScaffoldMessenger.of(cont).showSnackBar(bar);
    }
    return _status;
  }

  logout() {
    locator.get<StorageController>().removeAll();
    _auth.signOut();
  }

  Future<void> checkOfflineChanges() async {
    SharedPreferences.getInstance().then(
      (prefs) async {
        var newUsername = prefs.getString('user') ?? '';
        var newDiets = prefs.getString('diets') ?? '';
        if (newUsername != '') {
          locator.get<UserModel>().name = newUsername;
          await locator.get<DatabaseService>().changeInfo('name', newUsername);
        }
        if (newDiets != '') {
          List<String> toAdd = newDiets.split(',');
          locator.get<UserModel>().diets = toAdd;
          await locator.get<DatabaseService>().changeInfo('diets', toAdd);
        }
      },
    );
  }
}
