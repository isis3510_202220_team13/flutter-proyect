import 'dart:async';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:nutri_connect/locator.dart';
import 'package:nutri_connect/models/FoodInfo.dart';
import 'package:nutri_connect/models/Morsel.dart';
import 'package:openfoodfacts/openfoodfacts.dart';

import '../Services/open_food_facts.dart';
import '../Views/tabs/scanner_tab.dart';
import '../Views/utils/color_find.dart';

String nombre = "No connection established.";
String image = "http://via.placeholder.com/350x150";
Future<String> scanBarcodeNormal(BuildContext cont) async {
  // Platform messages may fail, so we use a try/catch PlatformException.
  String barcode = 'Not to be found';
  bool hasBeenNotFound = false;
  try {
    barcode = await FlutterBarcodeScanner.scanBarcode(
        '#ff6666', 'Cancel', true, ScanMode.BARCODE);
  } catch (e) {
    nombre = 'Failure to connect.';
  }
  try {
    String? tempNombre = "";
    String? tempImage = "";
    Product? temp = await getProduct(barcode);
    if (temp?.productName != null) {
      tempImage = temp?.imageFrontSmallUrl;
      tempNombre = temp?.productName;
    }
    if (tempNombre != null && tempImage != null) {
      nombre = tempNombre;
      image = tempImage;
    }
  } catch (e) {
    String errorCode = "Exception detected: $e";
    if (errorCode != "Exception detected: Exception: product not found.") {
      Food? savedFood = locator.get<FoodStorage>().giveFood(barcode);
      if (savedFood != null) {
        if (savedFood.barcode == 'Slipspace rupture') {
          hasBeenNotFound = true;
          showDialog(
              context: cont,
              builder: (cont) => AlertDialog(
                      title: const Text("You have connectivity issues!"),
                      content: const Text(
                          "You have connectivity issues and have not scanned this item before. Try again later."),
                      actions: [
                        TextButton(
                            child: const Text("Got it."),
                            onPressed: () {
                              Navigator.pop(cont);
                            }),
                      ]));
        } else {
          showDialog(
              context: cont,
              builder: (cont) => Dialog(
                  backgroundColor: Colors.transparent,
                  insetPadding: const EdgeInsets.all(10),
                  child: Stack(
                    alignment: Alignment.bottomCenter,
                    children: <Widget>[
                      Container(
                        width: double.infinity,
                        height: 300,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(15),
                            color: hexStringToColor("60992D")),
                        padding: const EdgeInsets.fromLTRB(20, 50, 20, 10),
                        child: Text("Product: ${savedFood.name}",
                            style: const TextStyle(fontSize: 24),
                            textAlign: TextAlign.center),
                      ),
                      Positioned(
                          top: 140,
                          width: 150,
                          height: 150,
                          child: CachedNetworkImage(
                            imageUrl: image,
                            progressIndicatorBuilder:
                                (context, url, downloadProgress) =>
                                    CircularProgressIndicator(
                                        value: downloadProgress.progress),
                            errorWidget: (context, url, error) =>
                                const Icon(Icons.error),
                          ))
                    ],
                  )));
        }
      }
    }
    nombre = 'Product not found.';
  }
  if (nombre != "Product not found.") {
    Food? finalFood = locator.get<FoodStorage>().giveFood(barcode);
    if (finalFood != null) {
      if (finalFood.barcode == 'Slipspace rupture') {
        Food toStore = Food(
            barcode: barcode,
            name: nombre,
            image: CachedNetworkImage(imageUrl: image));
        locator.get<FoodStorage>().addFood(toStore);
        finalFood = toStore;
      }
    }

    showDialog(
        context: cont,
        builder: (cont) => Dialog(
            backgroundColor: Colors.transparent,
            insetPadding: const EdgeInsets.all(10),
            child: Stack(
              alignment: Alignment.bottomCenter,
              children: <Widget>[
                Container(
                  width: double.infinity,
                  height: 300,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      color: hexStringToColor("60992D")),
                  padding: const EdgeInsets.fromLTRB(20, 50, 20, 10),
                  child: Text("Product: ${finalFood?.name}",
                      style: const TextStyle(fontSize: 24),
                      textAlign: TextAlign.center),
                ),
                Positioned(
                    top: 140,
                    width: 150,
                    height: 150,
                    child: CachedNetworkImage(
                      imageUrl: image,
                      progressIndicatorBuilder:
                          (context, url, downloadProgress) =>
                              CircularProgressIndicator(
                                  value: downloadProgress.progress),
                      errorWidget: (context, url, error) =>
                          const Icon(Icons.error),
                    ))
              ],
            )));
  } else if (!hasBeenNotFound) {
    showDialog(
        context: cont,
        builder: (cont) => AlertDialog(
                title: const Text("No product found!"),
                content: const Text(
                    "This product does not exist in the OpenFoodFacts database. No information can be shown."),
                actions: [
                  TextButton(
                      child: const Text("Got it."),
                      onPressed: () {
                        Navigator.pop(cont);
                      }),
                ]));
  }

  return Future(() => nombre);
}
