import 'package:shared_preferences/shared_preferences.dart';

class StorageController {
  Future<Map<String, dynamic>> getStringValues() async {
    final prefs = await SharedPreferences.getInstance();
    final keys = prefs.getKeys();
    final prefsMap = <String, dynamic>{};
    for (String key in keys) {
      prefsMap[key] = prefs.get(key);
    }
    return prefsMap;
  }

  Future<Map<String, dynamic>> getStringValuesMinusRemember() async {
    final prefs = await SharedPreferences.getInstance();
    final keys = prefs.getKeys();
    final prefsMap = <String, dynamic>{};
    for (String key in keys) {
      if (key != "password" && key != "remember_me" && key != "email") {
        prefsMap[key] = prefs.get(key);
      }
    }
    return Future(() => prefsMap);
  }

  Future<void> removePref(String key) async {
    var prefs = await SharedPreferences.getInstance();
    prefs.remove(key);
  }

  Future<void> removeAll() async {
    var prefs = await SharedPreferences.getInstance();
    var _email = prefs.getString("email") ?? "";
    var _password = prefs.getString("password") ?? "";
    var _rememberMe = prefs.getBool("remember_me") ?? false;

    prefs.clear();
    prefs.setBool("remember_me", _rememberMe);
    prefs.setString('email', _email);
    prefs.setString('password', _password);
  }
}
