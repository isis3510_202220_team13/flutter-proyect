import 'dart:io';

import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:nutri_connect/Services/database_service.dart';
import 'package:nutri_connect/Views/reusable_widgets/reusable.dart';
import 'package:nutri_connect/locator.dart';
import 'package:nutri_connect/models/user_model.dart';

class ProfileController {
  Future<String> changeUsername(BuildContext cont, userNow, userThen) async {
    if (userNow == userThen) {
      SnackBar bar = reusableSnackbar("That is already your username");
      ScaffoldMessenger.of(cont).showSnackBar(bar);
      return 'failed';
    }
    locator.get<UserModel>().changeName(userThen);
    String ans =
        await locator.get<DatabaseService>().changeInfo('name', userThen);
    if (ans == 'Error fetching user') {
      SnackBar bar =
          reusableSnackbar("No connection. Username saved locally only");
      ScaffoldMessenger.of(cont).showSnackBar(bar);
      return 'local';
    }
    SnackBar bar = reusableSnackbar("Username changed successfully");
    ScaffoldMessenger.of(cont).showSnackBar(bar);
    return 'success';
  }

  Future<void> pickUploadImage() async {
    try {
      final image = await ImagePicker().pickImage(
        source: ImageSource.gallery,
        maxWidth: 512,
        maxHeight: 512,
        imageQuality: 75,
      );

      Reference ref = FirebaseStorage.instance
          .ref()
          .child('${locator.get<UserModel>().email} profilepic.jpg');

      await ref.putFile(File(image!.path));

      ref.getDownloadURL().then((value) {
        print(value);
        locator.get<UserModel>().imagePath = value;
        locator.get<DatabaseService>().changeInfo('image', value);
      });
    } catch (e) {
      print('Error! $e');
    }
  }

  Future<String> takeUploadImage() async {
    try {
      final image = await ImagePicker().pickImage(
        source: ImageSource.camera,
        maxWidth: 512,
        maxHeight: 512,
        imageQuality: 75,
      );
      bool internet =
          await locator.get<DatabaseService>().checkUserConnection();
      print(internet);
      if (internet) {
        Reference ref = FirebaseStorage.instance
            .ref()
            .child('${locator.get<UserModel>().email} profilepic.jpg');

        await ref.putFile(File(image!.path));

        ref.getDownloadURL().then((value) {
          print(value);
          locator.get<UserModel>().imagePath = value;
          locator.get<DatabaseService>().changeInfo('image', value);
        });
        return 'success';
      }
      return 'failed';
    } catch (e) {
      print('Error! $e');
      return 'Error';
    }
  }
}
